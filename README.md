# My dmenu build

![Screenshot of my desktop](https://gitlab.com/nawo266/dmenu/-/raw/master/dmenu.jpg) 

## Used patches:
+ dmenu-alpha-20230110-5.2.diff
+ dmenu-barpadding-5.0_20210725_523aa08.diff
+ dmenu-center-5.2.diff
+ dmenu-fuzzymatch-4.9.diff
+ dmenu-mousesupport-5.2.diff
+ dmenu-mousesupport-motion-5.2.diff
+ dmenu-numbers-20220512-28fb3e2.diff
+ dmenu-xresources-4.9.diff
+ dmenu-xyw-5.0.diff
